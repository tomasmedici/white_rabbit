<?php

class WhiteRabbit2
{
    /**
     * return a php array, that contains the amount of each type of coin, required to fulfill the amount.
     * The returned array should use as few coins as possible.
     * The coins available for use is: 1, 2, 5, 10, 20, 50, 100
     * You can assume that $amount will be an int
     */
    public function findCashPayment($amount){
    	$arr1 = array();
    	$arr2 = (100, 50, 20, 10, 5, 2, 1);  		// creates an array of coin-values
    	foreach ($arr2 as $coin) {					// runs through this array and 	
    		if ($amount == 0) {						// checks if we are done
    			break;
    		}
    		else {
    		this -> intdiv ($amount, $coin);		// adds (each time in the beginning of the array) the number of the relevant coin needed
    		$arr1[] = "'$coin'" => this;			// don't know if you can add a variable inside quotes like this ;-)
    		$amount => $amount % $coin;
    		}
    	}
    	return $arr1;
    }
}