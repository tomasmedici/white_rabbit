<?php

class WhiteRabbit
{
    public function findMedianLetterInFile($filePath)
    {
        return array("letter"=>$this->findMedianLetter($this->parseFile($filePath),$occurrences),"count"=>$occurrences);
    }

    /**
     * Parse the input file for letters.
     * @param $filePath
     */
    private function parseFile ($filePath)
    {
       $file = file_get_contents ($filePath, true);								// Parses file into a string, maybe use FILE_USE_INCLUDE_PATH instead of true if version PHP 6 or later?
       return($file);
    }

    /**
     * Return the letter whose occurrences are the median.
     * @param $parsedFile
     * @param $occurrences
     */
    private function findMedianLetter($parsedFile, &$occurrences)
    {
        $arr1 = array();
        $letters = range('a' , 'z');
        $count = 0;
        foreach ($letters as $letter) {
        	$letterCount = substr_count ($parsedFile, $letter);					// For specific letter, count occurences in parsed file, DOES NOT COUNT CAPITAL LETTERS!! (to be adjusted)
        	if($letterCount != 0) {												// Only include non-zero occurences in array
        		$arr1[] = array("letter" => $letter, "count" => $letterCount);  // Adds to the array an array-entry with the required places filled and named
        		$count ++;     													// Increments the count, f.x. how many letters there are represented
        	}
        }
        $arr1_sort = array_sort($arr1, "count", SORT_ASC);						// Sorts by number of occurences, theoretically this could mean, that if ie. in text3 there is another letter with 2227 occurences, this might pop up as one of the medians, and the test will fail - this is highly unlikely, so is not taken into consideration here
        $value = $count / 2;													
        if($count % 2 = 0) {													// Checks if number of letters is even or odd, i.e. if there is one or two medians
        	return array($arr1_sort[$value], $arr1_sort[($value + 1)];			// Returns the relevant arrays containing the median and the count
        }  
        else {
        	return array($arr1_sort[($value + 0.5)];							// Returns the relevant array containing the median and the count
        }            
    }
}